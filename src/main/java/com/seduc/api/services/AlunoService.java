package com.seduc.api.services;

import com.seduc.api.entities.dtos.AlunoDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AlunoService {

    List<AlunoDTO> getAll();
    AlunoDTO saveOrUpdate(AlunoDTO aluno);
    void deleteById(Integer id);
    AlunoDTO getById(Integer id);
}
