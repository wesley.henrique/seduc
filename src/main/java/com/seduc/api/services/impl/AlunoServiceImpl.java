package com.seduc.api.services.impl;

import com.seduc.api.entities.Aluno;
import com.seduc.api.entities.dtos.AlunoDTO;
import com.seduc.api.exceptions.AlunoNotFoundException;
import com.seduc.api.repositories.AlunoRepository;
import com.seduc.api.services.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AlunoServiceImpl implements AlunoService {

    @Autowired
    AlunoRepository alunoRepository;

    @Override
    public List<AlunoDTO> getAll() {
        return alunoRepository.findAll().stream().map(Aluno::toDTO).collect(Collectors.toList());
    }

    @Override
    public AlunoDTO saveOrUpdate(AlunoDTO alunoDTO) {
        Aluno aluno = alunoRepository.save(alunoDTO.toEntity());
        return aluno.toDTO();
    }

    @Override
    public void deleteById(Integer id) {
        alunoRepository.deleteById(id);
    }

    @Override
    public AlunoDTO getById(Integer id) {
        return alunoRepository.findById(id).map(Aluno::toDTO).orElseThrow(() -> new AlunoNotFoundException(id));
    }
}
