package com.seduc.api.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.seduc.api.entities.dtos.AlunoDTO;
import com.seduc.api.entities.enums.Genero;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Aluno implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;

    @Enumerated(EnumType.ORDINAL)
    private Genero genero;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "turma_id")
    private Turma turma;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "ALUNO_DISCIPLINA",
                joinColumns = @JoinColumn(name = "aluno_id"),
                inverseJoinColumns = @JoinColumn(name = "disciplina_id")
    )
    private List<Disciplina> disciplinas = new ArrayList<>();

    public Aluno() {}

    public AlunoDTO toDTO() {
        AlunoDTO alunoDTO = new AlunoDTO();
        alunoDTO.setId(this.id);
        alunoDTO.setNome(this.nome);
        alunoDTO.setGenero(this.genero);

        return alunoDTO;
    }
}
