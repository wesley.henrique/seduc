package com.seduc.api.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Disciplina implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nome;

    @ManyToMany(mappedBy = "disciplinas")
    private List<Aluno> alunos = new ArrayList<>();

    @ManyToMany(mappedBy = "disciplinas")
    private List<Professor> professores = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "TURMA_DISCIPLINA",
              joinColumns = @JoinColumn(name = "disciplina_id"),
              inverseJoinColumns = @JoinColumn(name = "turma_id"))
    private List<Turma> turmas = new ArrayList<>();

    public Disciplina() {}
}
