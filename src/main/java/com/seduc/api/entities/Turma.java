package com.seduc.api.entities;

import com.seduc.api.entities.enums.Turno;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Turma implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "turma")
    private List<Aluno> alunos = new ArrayList<>();

    @ManyToMany(mappedBy = "turmas")
    private List<Professor> professores = new ArrayList<>();

    @ManyToMany(mappedBy = "turmas")
    private List<Disciplina> disciplinas = new ArrayList<>();

    @Enumerated(EnumType.ORDINAL)
    private Turno turno;

    public Turma() {}
}
