package com.seduc.api.entities.dtos;

import com.seduc.api.entities.Aluno;
import com.seduc.api.entities.enums.Genero;
import lombok.Data;

@Data
public class AlunoDTO {

    private Integer id;
    private String nome;
    private Genero genero;

    public AlunoDTO() {}

    public Aluno toEntity() {
        Aluno aluno = new Aluno();
        aluno.setId(this.id);
        aluno.setNome(nome);
        aluno.setGenero(this.genero);

        return aluno;
    }
}
