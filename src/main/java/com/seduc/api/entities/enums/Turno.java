package com.seduc.api.entities.enums;

public enum Turno {
    MANHA, TARDE, NOITE;
}
