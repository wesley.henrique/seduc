package com.seduc.api.entities.enums;

public enum Genero {
    MASCULINO, FEMININO
}
