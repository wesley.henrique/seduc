package com.seduc.api.exceptions;

public class AlunoNotFoundException extends RuntimeException {

    public AlunoNotFoundException(Integer id) {
        super("Aluno com id " + id + "não encontrado !");
    }
}
