package com.seduc.api.controllers;

import com.seduc.api.entities.dtos.AlunoDTO;
import com.seduc.api.services.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/alunos")
public class AlunoController {

    @Autowired
    AlunoService alunoService;

    @GetMapping
    private ResponseEntity<List<AlunoDTO>> getAll() {
        List<AlunoDTO> alunosDTO = alunoService.getAll();
        return ResponseEntity.ok(alunosDTO);
    }

    @GetMapping(value = "/{id}")
    private ResponseEntity<AlunoDTO> getById(@PathVariable("id") Integer id) {
        AlunoDTO alunoDTO = alunoService.getById(id);

        if (alunoDTO == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(alunoDTO);
    }

    @PostMapping
    private ResponseEntity<AlunoDTO> saveOrUpdate(@RequestBody AlunoDTO alunoDTO) {
        AlunoDTO alunoDTO1 = alunoService.saveOrUpdate(alunoDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(alunoDTO);
    }

    @DeleteMapping(value = "/{id}")
    private ResponseEntity delete(@PathVariable("id") Integer id) {
        this.alunoService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Deletado com sucesso");
    }
}
